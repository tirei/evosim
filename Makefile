# SPDX-FileCopyrightText: 2019 Timon Reinold <tirei@posteo.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

lint:
	isort -rc evosim
	black evosim
	reuse lint

run:
	python3 evosim

.PHONY: lint run
